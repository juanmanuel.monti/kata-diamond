import java.util.*

class DiamondApp {

    companion object {
        @JvmStatic
        fun main(args : Array<String>) {
            val reader = Scanner(System.`in`)
            println("Enter a letter to draw a Diamond: ")

            var diamond = Diamond(reader.next().single())

            diamond.print()
        }
    }
}