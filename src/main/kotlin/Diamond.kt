class Diamond(private val letter : Char) {

    private val firstLetter = 'A'

    fun rows():List<String>{
        if(letter == firstLetter)
            return listOf(firstLetter.toString())

        var rows = mutableListOf<String>()

        for(indexLetter in firstLetter..letter) {
            val row = rowFor(letter - firstLetter, indexLetter)
            rows.add(row)
        }
        return rows + rows.reversed().filterIndexed{ index, _ -> index >0}
    }

    fun print(){
        println(rows().joinToString(separator = "\n"))
    }

    private fun rowFor(diamondSize: Int, letter: Char): String {
        val letterDistance = letter - firstLetter
        val rowLength = diamondSize * 2 + 1
        return " ".repeat(rowLength)
            .replaceRange(diamondSize - letterDistance, diamondSize - letterDistance + 1, letter.toString())
            .replaceRange(rowLength / 2 + letterDistance, rowLength / 2 + letterDistance + 1, letter.toString())
    }
}