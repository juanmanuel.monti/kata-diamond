import junit.framework.Assert.assertEquals
import org.junit.Test

class PrintDiamondTest {

    @Test
    fun `print A diamond`() {
        val diamond = Diamond('A')
        val rows = diamond.rows()
        assertEquals(listOf("A"), rows)
    }

    @Test
    fun `print first row of B diamond`() {
        val diamond = Diamond('B')
        val rows = diamond.rows()
        assertEquals(" A ", rows[0])
    }

    @Test
    fun `print second row of B diamond`() {
        val diamond = Diamond('B')
        val rows = diamond.rows()
        assertEquals("B B", rows[1])
    }

    @Test
    fun `print c diamond`() {
        val diamond = Diamond('C')
        val rows = diamond.rows()
        println(rows)
        assertEquals("  A  ", rows[0])
        assertEquals(" B B ", rows[1])
        assertEquals("C   C", rows[2])
        assertEquals(" B B ", rows[3])
        assertEquals("  A  ", rows[4])
    }

    @Test
    fun `print d diamond`() {
        val diamond = Diamond('D')
        val rows = diamond.rows()
        println(rows)
        assertEquals("   A   ", rows[0])
        assertEquals("  B B  ", rows[1])
        assertEquals(" C   C ", rows[2])
        assertEquals("D     D", rows[3])
        assertEquals(" C   C ", rows[4])
        assertEquals("  B B  ", rows[5])
        assertEquals("   A   ", rows[6])
    }
}

